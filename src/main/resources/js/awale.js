const WEBSOCKET_URL = "ws://localhost:9000/ws";

var websocketConnection;
var playerName;

// Is it the player's turn or not?
var currentlyPlaying=false;

$(document).ready(function() {
    console.log("ready");

    websocketConnection = new WebSocket(WEBSOCKET_URL);

    websocketConnection.onopen = function(event){
        $("#connnectionStatus").text("Waiting for other player to arrive");
    }

    websocketConnection.onclose = function(event){
        $("#connnectionStatus").text("The connection has been closed");        
    }

    // Called every time we receive a new message
    websocketConnection.onmessage = function(event){
        const message = JSON.parse(event.data);

        if(message.messageType == "handshake"){
            handleHandshake(message);
        }
        else if(message.messageType == "turn"){
            handleTurn(message);
        }        
        else if(message.messageType == "illegal"){
            handleImpossibleMove(message.informationMessage);
        }
        else if(message.messageType == "tie"){
            handleTie(message);
        }
        else if(message.messageType == "win"){
            handleWin(message);
        }        
        else{
            console.log("Unknown message received. Ignoring");
            console.log(message);
        }
    }
});

/*
* What happens when the player clicks on one of the buttons. 
* User is playing his turn
*/
function play(pitNumber){
    console.log("Playing " + pitNumber);

    if(currentlyPlaying != true){
        console.log("Not your turn to play. Ignoring request");
        return;
    }

    currentlyPlaying = false;
    var message = {playerName: playerName, pitNumber: pitNumber};
    websocketConnection.send(JSON.stringify(message));
    
}

/*
* Handles behaviour when one of the players wins
*/
function handleTie(message){
    updateBoard(message.board);
    alert("Game finishes on a tie!");
}

/*
* Handles behaviour when one of the players wins
*/
function handleWin(message){
    updateBoard(message.board);
    alert(message.winner.name + " has won!");
}

/*
* Handles cases where the player has played a move that was not accepted
*/
function handleImpossibleMove(message){   
    alert("There was an issue with your last move. \n\n" + message + "\n\n Please try again");
}

/*
* Handles the first handshake message the client receives fromt he websocketConnection
*/
function handleHandshake(message){    
    playerName=message.player;
    $("#playerName").text(playerName);
}

/*
* Handles the messages indicating whose turn it is to play
*/
function handleTurn(message){
    $('#connnectionStatus').text("The game is on!");         

    var turnMessage;
    if(message.turnMessage == "you"){
        currentlyPlaying = true;
        turnMessage = "Your turn to play";
        updateBoard(message.board);
    }
    else if(message.turnMessage == "other"){
        currentlyPlaying = false;
        turnMessage = "Other player's turn to play";
        updateBoard(message.board);
    }
    else{
        console.log("Unknown turn message received. Ignoring");
        console.log(message);
    }
    
    $('#playerTurn').text(turnMessage);         
}

/*
* Updates the values of all the pits of the board
*/
function updateBoard(board){
    var playerSide = $.grep(board.sides, function(val, index){return val.owner.name === playerName})[0];
    var otherSide = $.grep(board.sides, function(val, index){return val.owner.name !== playerName})[0];

    updatePlayerPits(playerSide);
    updateOtherPits(otherSide);
}

/*
* Updates the values of the player pits
*/
function updatePlayerPits(side){
        $("#bigPit1").text(side.bigPit);

        $("#pit0").text(side.pits[0]);
        $("#pit1").text(side.pits[1]);
        $("#pit2").text(side.pits[2]);
        $("#pit3").text(side.pits[3]);
        $("#pit4").text(side.pits[4]);
        $("#pit5").text(side.pits[5]);
}

/*
* Updates the values of the opponent pits
*/
function updateOtherPits(side){
        $("#bigPit2").text(side.bigPit);

        $("#pit6").text(side.pits[0]);
        $("#pit7").text(side.pits[1]);
        $("#pit8").text(side.pits[2]);
        $("#pit9").text(side.pits[3]);
        $("#pit10").text(side.pits[4]);
        $("#pit11").text(side.pits[5]);
}