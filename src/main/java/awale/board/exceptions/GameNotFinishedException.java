package awale.board.exceptions;

/**
 * An exception that happens when someone asks for the result of an unfinished game
 *
 * @author jll
 *
 */
public class GameNotFinishedException extends Exception {

	private static final long serialVersionUID = 1L;

	public GameNotFinishedException() {
	}
}
