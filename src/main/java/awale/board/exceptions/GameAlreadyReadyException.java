package awale.board.exceptions;

/**
 * An exception that happens when someone asks for the result of an unfinished game
 *
 * @author jll
 *
 */
public class GameAlreadyReadyException extends Exception {

	private static final long serialVersionUID = 1L;

	public GameAlreadyReadyException() {
	}
	
	public GameAlreadyReadyException(String message) {
		super(message);
	}
}
