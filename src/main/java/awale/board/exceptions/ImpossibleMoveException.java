package awale.board.exceptions;

/**
 * An exception that happens when the user tries to do something that is not possible
 *
 * @author jll
 *
 */
public class ImpossibleMoveException extends Exception {

	private static final long serialVersionUID = 1L;

	public ImpossibleMoveException(String message) {
		super(message);
	}
}
