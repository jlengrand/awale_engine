package awale.board.player;

import java.util.logging.Logger;

/**
 * A simple class that represents a player in the game
 *
 * @author jleng
 *
 */
public class Player {

	private final static Logger LOGGER = Logger.getLogger(Player.class.getName());

	private String name;
	private int score = 0;

	public Player() {
		this("NoName");
	}

	public Player(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public void addToScore(int points) {
		if (points < 0) {
			LOGGER.warning("Player's score can only be increased! Ignoring request");
			return;
		}
		score += points;
	}

	@Override
	public String toString() {
		return "Player is " + name + " with a score of " + score;
	}

}
