package awale.board;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import awale.board.exceptions.GameAlreadyReadyException;
import awale.board.exceptions.GameNotFinishedException;
import awale.board.exceptions.ImpossibleMoveException;
import awale.board.player.Player;

public class Board {

	private final static int DEFAULT_STARTING_TOKENS = 6;

	private final static Logger LOGGER = Logger.getLogger(Board.class.getName());

	private List<Side> sides;
	private boolean ready = false;
	private Player playerTurn;
	private List<Player> players;
	private int startingTokens = DEFAULT_STARTING_TOKENS;

	private AwaleMechanics awaleMechanics = new AwaleMechanics();

	public Board(List<Player> players, int startingTokens) {
		this.players = players;
		this.startingTokens = startingTokens;
	}

	public Board(List<Player> players) {
		this(players, DEFAULT_STARTING_TOKENS);
	}

	public Board() {
		this(new ArrayList<Player>(), DEFAULT_STARTING_TOKENS);
	}

	/**
	 * Notifies the game engine that the game is ready to be played. All players are here.
	 * @throws GameAlreadyReadyException 
	 */
	public void ready() throws GameAlreadyReadyException {
		if(ready){
			throw new GameAlreadyReadyException("Cannot call ready again");
		}
		playerTurn = players.get(0);

		sides = players.stream().map(p -> new Side(p, startingTokens)).collect(Collectors.toList());
		awaleMechanics.setSides(sides);

		ready = true;
	}

	/**
	 * Current player plays a move
	 *
	 * @param index is the index of the pit the user wants to play. First pit has index 0
	 * @throws ImpossibleMoveException
	 */
	public boolean play(int index) throws ImpossibleMoveException {
		Side side = getSideFromOwner();

		boolean lastTokenOnBigPit = awaleMechanics.moveTokens(side, index);

		if (isGameFinished()) {
			awaleMechanics.cleanPits();
		}

		updatePlayerTurn(lastTokenOnBigPit);
		updatePlayersScores();

		return isGameFinished();
	}

	/**
	 * Updates the players scores to match their big pits's tokens
	 */
	private void updatePlayersScores() {
		for (Side side : sides) {
			side.getOwner().setScore(side.getBigPit());
		}
	}

	public Player getPlayerTurn() {
		return playerTurn;
	}

	/**
	 * Returns the player that will play next.
	 *
	 * @return the player that will play next
	 */
	public Player getNextPlayerTurn() {
		int index = players.indexOf(playerTurn);
		if (index < 0 || (index + 1) == players.size()) {
			return players.get(0);
		}
		else {
			return players.get(index + 1);
		}
	}

	/**
	 * Updates which player should play next.
	 *
	 * @param boolean to indicate whether the last token was on placed on a plyer's big pit.
	 */
	private void updatePlayerTurn(boolean lastTokenOnBigPit) {
		// Player gets another turn.
		if (lastTokenOnBigPit) {
			return;
		}

		playerTurn = getNextPlayerTurn();
	}

	/**
	 * Returns True when one of the players has won.
	 *
	 * @param true is the game has been won
	 */
	private boolean isGameFinished() {
		return sides.stream().anyMatch(Side::isSideEmpty);
	}

	/**
	 * Checks if the current game ended with a tie
	 *
	 * @return true in case both players ended with the same score
	 */
	private boolean isTie() {
		if (!isGameFinished()) {
			return false;
		}

		List<Integer> scores = players.stream().map(Player::getScore).collect(Collectors.toList());
		return scores.stream().distinct().count() == 1;

	}

	@Override
	public String toString() {
		String result = "Playing now : " + getPlayerTurn().getName() + System.getProperty("line.separator");

		if (sides.size() == 2) {
			// Less worse visualization for 2 players
			result += players.get(0) + " - " + sides.get(0).toString(true) + System.getProperty("line.separator");
			result += players.get(1) + " - " + sides.get(1) + System.getProperty("line.separator");
		}
		else {
			for (int i = 0; i < players.size(); i++) {
				result += players.get(i) + " - " + sides.get(i) + System.getProperty("line.separator");
			}
		}

		return result;
	}

	/**
	 * Returns the first side that belongs to a given owner. 
	 *
	 * @return the side that belongs to a given user. Returns null if no side has been found
	 */
	private Side getSideFromOwner() {
		for (Side side : sides) {
			if (side.getOwner().getName().equals(playerTurn.getName())) {
				return side;
			}
		}
		return null;
	}

	/**
	 * Adds a new player to the Board. Should only be done before the game is ready to be started
	 *
	 * @param player the player to add to the table
	 */
	public void addPlayer(Player player) {
		if (isReady()) {
			LOGGER.warning("Cannot add new player on a game that is already running.");
		}
		players.add(player);
	}

	public boolean isReady() {
		return ready;
	}

	/**
	 * Returns the player that has won the game.
	 * If the game is not finished or is a tie, returns null.
	 *
	 * @return the Player that has won the game. Null if the game is a tie
	 * @throws a GameNotFinishedException in case there is no winner yet!
	 */
	public Player getWinner() throws GameNotFinishedException {
		if (!isGameFinished()) {
			throw new GameNotFinishedException();
		}
		if (isTie()) {
			return null;
		}

		return Collections.max(players, Comparator.comparing(c -> c.getScore()));
	}
}
