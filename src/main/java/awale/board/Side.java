/**
 *
 */
package awale.board;

import java.util.Arrays;
import java.util.logging.Logger;
import org.apache.commons.lang3.ArrayUtils;
import awale.board.player.Player;

/**
 * A class representing a single side of the board game.
 * A side belongs to a single Player
 *
 * @author jlengrand
 *
 */
public class Side {

	private final static Logger LOGGER = Logger.getLogger(Side.class.getName());

	/* We might want to play with more pits in the future :) */
	public static final int NUMBER_PITS = 6;

	/* We separate him from the others because it behaves slightly different. */
	private int bigPit = 0;

	private int[] pits;

	/* The owner of the side */
	private Player owner;

	public Side(Player owner, int[] pits) {
		this.owner = owner;
		this.pits = Arrays.copyOf(pits, pits.length);
	}

	public Side(Player owner, int startingTokens) {
		initSide(owner, startingTokens);
	}

	/**
	 * Initializes the game side. Creates the relevant number of pits and fills them up with tokens
	 *
	 * @param startingTokens the number of tokens to place in each pit
	 */
	private void initSide(Player owner, final int startingTokens) {
		this.owner = owner;
		pits = new int[NUMBER_PITS];
		pits = Arrays.stream(pits).map(n -> startingTokens).toArray();
	}

	public int[] getPits() {
		return pits;
	}

	public int getBigPit() {
		return bigPit;
	}

	public void addToBigPit() {
		addToBigPit(1);
	}

	public void addToBigPit(int points) {
		if (points < 0) {
			LOGGER.warning("Player's score can only be increased! Ignoring request");
			return;
		}
		bigPit += points;
	}

	public Player getOwner() {
		return owner;
	}

	public boolean isSideEmpty() {
		return Arrays.stream(pits).sum() == 0;
	}

	/**
	 * Checks if the owner of the side is a given player
	 *
	 * @param p the player to check against
	 * @return true if the side is owned by the input player
	 */
	public boolean belongsToPlayer(Player p) {

		return owner.equals(p);
	}

	@Override
	public String toString() {
		return toString(false);
	}

	/**
	 * A not-default toString method that allows to see the player's pits reversed. Not used by default but can be used for convenience.
	 *
	 * @param reversed reverse the output. Can be useful to display side information for an opponent
	 * @return a vizualization of the side
	 */
	public String toString(boolean reversed) {
		if (reversed) {
			int[] reversedPits = Arrays.copyOf(pits, pits.length);
			ArrayUtils.reverse(reversedPits);
			return "[" + bigPit + "] " + Arrays.toString(reversedPits);
		}
		else {
			return Arrays.toString(pits) + " [" + bigPit + "]";
		}
	}
}
