/**
 *
 */
package awale.board;

import java.util.List;
import java.util.logging.Logger;
import awale.board.exceptions.ImpossibleMoveException;
import awale.board.player.Player;

/**
 * A class that contains all of the Awale mechanics
 * Methods are public so they are available to the engine.
 *
 * @author jlengrand
 *
 */
public class AwaleMechanics {

	private final static Logger LOGGER = Logger.getLogger(AwaleMechanics.class.getName());

	private List<Side> sides;

	public AwaleMechanics() {
	}

	/**
	 * Handles the complete logic of a player moving tokens around
	 *
	 * @param s the side from which tokens are being removed
	 * @param index the index of the pit to pick tokens from
	 * @return true if the last token was placed in a big pit
	 * @throws ImpossibleMoveException
	 */
	public boolean moveTokens(Side s, int index) throws ImpossibleMoveException {
		if (index < 0 || index > Side.NUMBER_PITS - 1) {
			throw new ImpossibleMoveException("Player asking to move tokens from an unexistant pit.");
		}

		if (s.getPits()[index] == 0) {
			throw new ImpossibleMoveException("Cannot play a move on an empty pit.");
		}

		Player playerTurn = s.getOwner();

		int tokens = s.getPits()[index];
		s.getPits()[index] = 0;

		int ptr = index + 1;
		while (tokens > 0) {
			// Current player's pits
			if (ptr < Side.NUMBER_PITS) {
				s.getPits()[ptr] += 1;
				ptr += 1;
				tokens -= 1;

				// Last token ends on an own empty pit
				if (tokens == 0 && s.belongsToPlayer(playerTurn) && s.getPits()[ptr - 1] == 1) {
					LOGGER.info("Good move. Stealing other player's tokens.");

					s.getPits()[ptr - 1] = 0;
					s.addToBigPit();
					// Steals other player's token
					Side opponentSide = changeSide(s);
					int stolenTokens = opponentSide.getPits()[Side.NUMBER_PITS - ptr];
					opponentSide.getPits()[Side.NUMBER_PITS - ptr] = 0;
					s.addToBigPit(stolenTokens);
				}
			}
			else {
				if (s.belongsToPlayer(playerTurn)) {
					// Increasing Big Pit
					s.addToBigPit();
					tokens -= 1;

					if (tokens == 0) {
						return true;
					}
				}
				// We switch side and reinitializes the ptr.
				s = changeSide(s);
				ptr = 0;
			}
		}

		return false;
	}

	/**
	 * When a game has been finished, gathers all player's tokens into their big pit.
	 */
	public void cleanPits() {
		for (Side s : sides) {
			for (int i = 0; i < s.getPits().length; i++) {
				int tokens = s.getPits()[i];
				s.addToBigPit(tokens);
				s.getPits()[i] = 0;
			}
		}
	}

	/**
	 * Method that selects the next side after a given side in the list of sides
	 *
	 * @param side the side to use as reference
	 * @return the next side in the list
	 */
	private Side changeSide(Side side) {
		int sideIndex = sides.indexOf(side);
		if (sideIndex == -1) {
			LOGGER.severe("Caught negative index when selecting the next side to play. Falling back to the first side.");
			side = sides.get(0);
		}
		else {
			side = sideIndex == sides.size() - 1 ? sides.get(0) : sides.get(sideIndex + 1);
		}
		return side;
	}

	public List<Side> getSides() {
		return sides;
	}

	public void setSides(List<Side> sides) {
		this.sides = sides;
	}
}
