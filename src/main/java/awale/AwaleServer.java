/**
 *
 */
package awale;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import org.glassfish.grizzly.http.server.CLStaticHttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import awale.server.netty.WebSocketServerPipelineFactory;

/**
 * A server hosting games of Awale and allowing people to play against each other on different computers
 * Created with inspiration from <a href="http://kevinwebber.ca/multiplayer-tic-tac-toe-in-java-using-the-websocket-api-netty-nio-and-jquery/">Kevin Webber</a>
 *
 * @author Julien Lengrand-Lambert
 */
public class AwaleServer {

	public static void main(String[] args) throws IOException {
        serveStaticFiles();
		handleWebsocketConnection();
	}

	/**
	 * Sets up a server that will handle our websocket between the clients.
	 */
	private static void handleWebsocketConnection() {
		ChannelFactory factory = new NioServerSocketChannelFactory(Executors.newCachedThreadPool(), Executors.newCachedThreadPool());
		ServerBootstrap bootstrap = new ServerBootstrap(factory);

		bootstrap.setPipelineFactory(new WebSocketServerPipelineFactory());
		bootstrap.setOption("child.tcpNoDelay", true);
		bootstrap.setOption("child.keepAlive", true);
		bootstrap.bind(new InetSocketAddress(9000));
		System.out.println("Awale Server: Websocket listening on port 9000");
	}

	/**
	 * Sets up a Web Server that will handle our static files and respond to HTTP requests
	 */
	private static void serveStaticFiles() throws IOException {
		HttpServer httpServer = new HttpServer();
		NetworkListener listener = new NetworkListener("grizzly-static", "0.0.0.0", 8080);
        httpServer.addListener(listener);

        httpServer.getServerConfiguration().addHttpHandler(new CLStaticHttpHandler(AwaleServer.class.getClassLoader()), "/");
        httpServer.start();
	}

}
