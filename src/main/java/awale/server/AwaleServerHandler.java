/**
 *
 */
package awale.server;

import java.util.HashMap;
import java.util.logging.Logger;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.jboss.netty.handler.codec.http.DefaultHttpResponse;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.jboss.netty.handler.codec.http.HttpHeaders.Names;
import org.jboss.netty.handler.codec.http.HttpHeaders.Values;
import org.jboss.netty.handler.codec.http.HttpMethod;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpResponse;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.jboss.netty.handler.codec.http.HttpVersion;
import org.jboss.netty.handler.codec.http.websocketx.CloseWebSocketFrame;
import org.jboss.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.jboss.netty.handler.codec.http.websocketx.WebSocketFrame;
import org.jboss.netty.handler.codec.http.websocketx.WebSocketServerHandshaker;
import org.jboss.netty.handler.codec.http.websocketx.WebSocketServerHandshakerFactory;
import com.google.gson.Gson;
import awale.board.Board;
import awale.board.exceptions.GameAlreadyReadyException;
import awale.board.exceptions.GameNotFinishedException;
import awale.board.exceptions.ImpossibleMoveException;
import awale.board.player.Player;
import awale.server.exceptions.UnknownMessageException;
import awale.server.messages.HandshakeMessage;
import awale.server.messages.IllegalMoveMessage;
import awale.server.messages.OtherTurnMessage;
import awale.server.messages.TieMessage;
import awale.server.messages.TurnInformationMessage;
import awale.server.messages.WinMessage;
import awale.server.messages.YourTurnMessage;

/**
 * Specific handler meant to allow for the Awale game to be played using Websockets.
 * Handles all of the specific client/server communication
 *
 * @author Julien Lengrand-Lambert
 *
 */
public class AwaleServerHandler extends SimpleChannelUpstreamHandler {

	private final static Logger LOGGER = Logger.getLogger(AwaleServerHandler.class.getName());
	private static final String WEBSOCKET_PATH = "/ws";

	private Gson gson = new Gson();
	
	// A map of channels attached to connected players, to know what to send to who
	private static HashMap<String, Channel> playerChannels = new HashMap<>();
	private static Board board;

	/*
	 * (non-Javadoc)
	 * @see org.jboss.netty.channel.SimpleChannelUpstreamHandler#messageReceived(org.jboss.netty.channel.ChannelHandlerContext, org.jboss.netty.channel.MessageEvent)
	 * This method will be called every time a message is received. We can receive two diferent types of messages.
	 */
	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
		Object message = e.getMessage();
		if (message instanceof HttpRequest) {
			handleHttpRequest(ctx, (HttpRequest) message);
		}
		else if (message instanceof WebSocketFrame) {
			handleWebSocketFrameRequest(ctx, (WebSocketFrame) message);
		}
		else {
			throw new UnknownMessageException("The server received a message of an unknown type");
		}
	}

	/**
	 * This method will be called every time an HTTP Request is received by the server.
	 * The aim here is to create a handshake with the other party so that a websocket connection can be created
	 *
	 * @param context ChannelHandlerContext current context coming from the channel handler of the server
	 * @param request HTTP Request to be processed
	 */
	private void handleHttpRequest(ChannelHandlerContext context, HttpRequest request) {
		if (!isGetRequest(request) || !isWebSocketConnection(request)) {
			sendHttpResponse(context, new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.FORBIDDEN));
			return;
		}

		WebSocketServerHandshakerFactory websocketFactory = new WebSocketServerHandshakerFactory(getWebSocketLocation(request), null, false);
		WebSocketServerHandshaker handshaker = websocketFactory.newHandshaker(request);

		if (handshaker == null) {
			websocketFactory.sendUnsupportedWebSocketVersionResponse(context.getChannel());
		}
		else {
			handshaker.handshake(context.getChannel(), request);
			initializeBoard(context);
		}
	}

	/**
	 * This method will be called every time an WebSocketFrame is received by the server.
	 * These messages will contain the information coming from the players directly as they play the game.
	 *
	 * @param context ChannelHandlerContext current context coming from the channel handler of the server
	 * @param message WebSocketFrame to be processed
	 */
	private void handleWebSocketFrameRequest(ChannelHandlerContext context, WebSocketFrame message) {

		if (message instanceof TextWebSocketFrame) {
			TextWebSocketFrame frame = (TextWebSocketFrame) message;
			TurnInformationMessage turnMessage = gson.fromJson(frame.getText(), TurnInformationMessage.class);

			boolean gameFinished = false;
			try {
				gameFinished = board.play(turnMessage.getPitNumber());
			}
			catch (ImpossibleMoveException exception) {
				LOGGER.warning("Illegal move played. Refusing move");
				playerChannels.get(board.getPlayerTurn().getName()).write(new TextWebSocketFrame(new IllegalMoveMessage(exception.getMessage()).toJson()));
			}

			// Victory?
			if (gameFinished) {
				try {
					Player winner = board.getWinner();
					if (winner == null) {
						playerChannels.values().forEach(channel -> channel.write(new TextWebSocketFrame(new TieMessage(board).toJson())));
					}
					else {
						playerChannels.values().forEach(channel -> channel.write(new TextWebSocketFrame(new WinMessage(board, winner).toJson())));
					}
				}
				catch (GameNotFinishedException exception) {
					LOGGER.warning("Game finished but no winner found.");
				}
				LOGGER.info("The game is finished");
			}
			else {
				// We update player's turns
				playerChannels.get(board.getPlayerTurn().getName()).write(new TextWebSocketFrame(new YourTurnMessage(board).toJson()));
				playerChannels.get(board.getNextPlayerTurn().getName()).write(new TextWebSocketFrame(new OtherTurnMessage(board).toJson()));
			}
		}
		else {
			LOGGER.warning("Unknown type of WebSocketFrame received. Ignoring");
		}
	}

	/**
	 * Initializes a game. Finds an open game for a player (if another player is already waiting) or creates a new game.
	 *
	 * @param context ChannelHandlerContext current context coming from the channel handler of the server
	 */
	private void initializeBoard(ChannelHandlerContext context) {
		Player player;
		if (board == null) {
			// First player arrives
			board = new Board();
			player = new Player("Player 1");
			board.addPlayer(player);
		}
		else {			
			// Second player is here too!

			player = new Player("Player 2");
			board.addPlayer(player);
			try {
				board.ready();
			}
			catch (GameAlreadyReadyException e) {
				//Another (third?) player coming in
				LOGGER.warning("Another player trying to enter the field. Ignoring request.");
				return;
			}
		}
		playerChannels.put(player.getName(), context.getChannel());

		// Send confirmation message to player with its name
		context.getChannel().write(new TextWebSocketFrame(new HandshakeMessage(player.getName()).toJson()));

		// If the game has begun we need to inform the players. Send them a "turn" message (either "waiting" or "your_turn")
		if (board.isReady()) {
			playerChannels.get(board.getPlayerTurn().getName()).write(new TextWebSocketFrame(new YourTurnMessage(board).toJson()));
			playerChannels.get(board.getNextPlayerTurn().getName()).write(new TextWebSocketFrame(new OtherTurnMessage(board).toJson()));
		}

	}

	// ----------
	// Utilities. Should probably be refactored later
	// ----------

	/**
	 * Checks that the incoming request is an HTTP Get request
	 *
	 * @param message Incoming HTTP request
	 * @return true if the message is a GET request
	 */
	private boolean isGetRequest(HttpRequest message) {
		return (message.getMethod() == HttpMethod.GET);
	}

	/**
	 * Checks that the incoming request is a request towards the Websocket Uri
	 *
	 * @see http://enterprisewebbook.com/ch8_websockets.html#HANDSHAKE for more information
	 *
	 * @param context Current context of the channel handler
	 * @return true if the request requests for an upgrade to a websocket connection
	 */
	private boolean isWebSocketConnection(HttpRequest request) {
		return (request.getUri().equals(WEBSOCKET_PATH)
				&& requestHeaderIsWebsocketUpgrade(request));
	}

	/**
	 * Ensures that the HTTP Request is a request for a websocket upgrade
	 *
	 * @param request the incoming HTTP Request
	 * @return true if the request is an upgrade request for a websocket connection
	 */
	private boolean requestHeaderIsWebsocketUpgrade(HttpRequest request) {
		return Values.WEBSOCKET.equalsIgnoreCase(request.getHeader(Names.UPGRADE)) &&
				Values.UPGRADE.equalsIgnoreCase(request.getHeader(Names.CONNECTION));
	}

	/**
	 * Sends an HTTP Response on the network
	 *
	 * @param context Current context of the channel handler
	 * @param response HTTP Response to be sent back
	 */
	private void sendHttpResponse(ChannelHandlerContext context, HttpResponse response) {
		context.getChannel().write(response);
	}

	/**
	 * Returns the websocket host location
	 *
	 * @param request incoming HTTP request
	 * @return the webSocketLocation as a String
	 */
	private String getWebSocketLocation(HttpRequest request) {
		return "ws://" + request.getHeader(HttpHeaders.Names.HOST) + WEBSOCKET_PATH;
	}

}
