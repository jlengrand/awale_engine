/**
 *
 */
package awale.server.messages;

/**
 * Message sent when the websocket initializes, to handle handshaking
 * @author Julien Lengrand-Lambert
 *
 */
public class HandshakeMessage extends AbstractJsonMessage {

	public static final String MESSAGE_TYPE = "handshake";
	private String player;

	public HandshakeMessage(String player) {
		super();
		this.player = player;
	}

	@Override
	public String getMessageType() {
		return MESSAGE_TYPE;
	}

	public String getPlayer() {
		return player;
	}

}
