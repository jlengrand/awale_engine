/**
 *
 */
package awale.server.messages;

import awale.board.Board;

/**
 * It is your turn to play
 *
 * @author Julien Lengrand-Lambert
 *
 */
public class YourTurnMessage extends AbstractTurnMessage {

	public YourTurnMessage(Board board) {
		super(board);
	}

	public static final String TURN_MESSAGE = "you";

	/*
	 * (non-Javadoc)
	 * @see awale.server.messages.AbstractTurnMessage#getTurnMessage()
	 */
	@Override
	public String getTurnMessage() {
		return TURN_MESSAGE;
	}

}
