/**
 *
 */
package awale.server.messages;

import awale.board.Board;
import awale.board.player.Player;

/**
 * Message sent to the clients when one of them won
 *
 * @author Julien Lengrand-Lambert
 *
 */
public class WinMessage extends AbstractJsonMessage {

	public static final String MESSAGE_TYPE = "win";
	private Board board;
	private Player winner;

	public WinMessage(Board board, Player winner) {
		super();
		this.board = board;
		this.winner = winner;
	}

	@Override
	public String getMessageType() {
		return MESSAGE_TYPE;
	}

	public Board getBoard() {
		return board;
	}

	public Player getWinner() {
		return winner;
	}
}
