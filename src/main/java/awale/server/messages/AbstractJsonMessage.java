/**
 *
 */
package awale.server.messages;

import com.google.gson.Gson;

/**
 * A sample Json message. Meant to be extended, and ensures children have a message type value
 *
 * @author Julien Lengrand-Lambert
 *
 */
public abstract class AbstractJsonMessage {

	@SuppressWarnings("unused")
	private String messageType;

	public AbstractJsonMessage() {
		messageType = getMessageType();
	}

	/**
	 * Returns the object as a Json string
	 *
	 * @return
	 */
	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

	public abstract String getMessageType();
}
