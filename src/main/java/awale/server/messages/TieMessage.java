/**
 *
 */
package awale.server.messages;

import awale.board.Board;

/**
 * Message being sent to the clients when the game ends in a tie
 *
 * @author Julien Lengrand-Lambert
 *
 */
public class TieMessage extends AbstractJsonMessage {

	public static final String MESSAGE_TYPE = "tie";
	private Board board;

	public TieMessage(Board board) {
		super();
		this.board = board;
	}

	@Override
	public String getMessageType() {
		return MESSAGE_TYPE;
	}

	public Board getBoard() {
		return board;
	}
}
