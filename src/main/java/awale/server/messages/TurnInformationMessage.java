package awale.server.messages;

/**
 * Contains information coming from a client about the turn they want to play.
 *
 * @author Julien Lengrand-Lambert
 *
 */
public class TurnInformationMessage {

	private String playerName;
	private int pitNumber;

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public int getPitNumber() {
		return pitNumber;
	}

	public void setPitNumber(int pitNumber) {
		this.pitNumber = pitNumber;
	}

}
