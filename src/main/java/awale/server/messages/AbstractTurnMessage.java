/**
 *
 */
package awale.server.messages;

import awale.board.Board;

/**
 * A message to indicate whether it is your turn or not. Meant to be extended to provide more precise information.
 *
 * @author Julien Lengrand-Lambert
 *
 */
public abstract class AbstractTurnMessage extends AbstractJsonMessage {

	public static final String MESSAGE_TYPE = "turn";

	private Board board;

	@SuppressWarnings("unused")
	private String turnMessage;

	public AbstractTurnMessage(Board board) {
		super();
		this.board = board;
		turnMessage = getTurnMessage();
	}

	@Override
	public String getMessageType() {
		return MESSAGE_TYPE;
	}

	public abstract String getTurnMessage();

	public Board getBoard() {
		return board;
	}
}
