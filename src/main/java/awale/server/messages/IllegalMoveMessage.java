/**
 *
 */
package awale.server.messages;

/**
 * Message sent to players when their turn was not possible.
 *
 * @author Julien Lengrand-Lambert
 *
 */
public class IllegalMoveMessage extends AbstractJsonMessage {

	public static final String ILLEGAL_MESSAGE = "illegal";

	private String informationMessage;

	public IllegalMoveMessage(String informationMessage) {
		super();
		this.informationMessage = informationMessage;
	}

	@Override
	public String getMessageType() {
		return ILLEGAL_MESSAGE;
	}

	public String getInformationMessage() {
		return informationMessage;
	}
}
