/**
 *
 */
package awale.server.messages;

import awale.board.Board;

/**
 * It is the other player's turn to play
 *
 * @author Julien Lengrand-Lambert
 *
 */
public class OtherTurnMessage extends AbstractTurnMessage {

	public static final String TURN_MESSAGE = "other";

	public OtherTurnMessage(Board board) {
		super(board);
	}

	@Override
	public String getTurnMessage() {
		return TURN_MESSAGE;
	}

}
