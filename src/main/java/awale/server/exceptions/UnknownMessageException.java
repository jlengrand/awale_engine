package awale.server.exceptions;

/**
 * An exception that happens when a server handler receives a mesage that it cannot understand
 *
 * @author jll
 *
 */
public class UnknownMessageException extends Exception {

	private static final long serialVersionUID = 1L;

	public UnknownMessageException(String message) {
		super(message);
	}
}
