package awale;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import awale.board.Board;
import awale.board.exceptions.GameAlreadyReadyException;
import awale.board.exceptions.GameNotFinishedException;
import awale.board.exceptions.ImpossibleMoveException;
import awale.board.player.Player;

/**
 * A simple way to play Awale from the command line
 *
 * @author Julien Lengrand-Lambert
 *
 */
public class AwaleCLI {

	public static Scanner keyboard = new Scanner(System.in);

	public static void main(String[] args) {
		boolean gameFinished = false;

		Player p1 = new Player("P1");
		Player p2 = new Player("P2");

		List<Player> players = new ArrayList<>(Arrays.asList(p1, p2));
		Board board = new Board(players);

		System.out.println("Ready to rumble?");
		try{
			board.ready();			
		}
		catch(GameAlreadyReadyException exception){
			System.out.println("Game already ready. Continuing.");
		}

		while (gameFinished != true) {
			System.out.println(board);

			System.out.println("Waiting for input from :" + board.getPlayerTurn().getName());
			System.out.println("Please enter which pit to play (between 0 and 5)");

			int index = keyboard.nextInt();
			try {
				gameFinished = board.play(index);
			}
			catch (ImpossibleMoveException e) {
				System.out.println("The input entered was incorrect, please try again");
			}
		}

		System.out.println("Game is finished!");
		try {
			Player player = board.getWinner();
			if (player == null) {
				System.out.println("The game ended in a tie!");
			}
			else {
				System.out.println("Winner is " + player.getName() + "!");
			}
		}
		catch (GameNotFinishedException e) {
			System.out.println("The game is finished, this should not happen");

		}
	}

	public static int getKeyboardInput() {

		return keyboard.nextInt();
	}
}
