/**
 *
 */
package awale.board;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import awale.board.player.Player;

/**
 * Tests for the Side class
 *
 * @author jlengrand
 *
 */
public class SideTest {

	private static final int STARTING_TOKENS = 6;
	public Side side;
	public Player player;

	@Before
	public void setUp() throws Exception {
		player = new Player();
		side = new Side(player, STARTING_TOKENS);
	}

	@Test
	public void testInitSide() {
		assertTrue(Arrays.stream(side.getPits()).allMatch(s -> s == STARTING_TOKENS));
	}

	@Test
	public void testSideEmpty() {
		Side side0 = new Side(new Player(), 0);
		assertTrue(side0.isSideEmpty());
	}

	@Test
	public void testBelongsToPlayer() {
		assertTrue(side.belongsToPlayer(player));
		assertFalse(side.belongsToPlayer(new Player()));
	}

	@Test
	public void testAddToBigPit() {
		assertEquals(0, side.getBigPit());

		side.addToBigPit();
		assertEquals(1, side.getBigPit());

		side.addToBigPit(-5);
		assertEquals(1, side.getBigPit());

		side.addToBigPit(5);
		assertEquals(6, side.getBigPit());
	}
}
