/**
 *
 */
package awale.board;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import awale.board.exceptions.ImpossibleMoveException;
import awale.board.player.Player;

/**
 * @author Julien Lengrand-Lambert
 *
 */
public class AwaleMechanicsTest {

	public static final int[] SETUP_NORMAL = { 3, 0, 0, 1, 0, 0 };
	public static final int[] SETUP_EMPTY = { 0, 0, 0, 0, 0, 0 };
	public static final int[] SETUP_BIGPIT = { 6, 0, 0, 1, 0, 0 };
	public static final int[] SETUP_OTHER_PLAYER = { 1, 0, 0, 0, 0, 8 };
	public static final int[] SETUP_ENDS_EMPTY = { 0, 0, 0, 0, 0, 8 };
	public static final int[] SETUP_ENDS_EMPTY_P2 = { 0, 0, 0, 0, 0, 4 };
	public static final int[] SETUP_CLEAN_P1 = { 1, 2, 3, 4, 5, 6 };
	public static final int[] SETUP_CLEAN_P2 = { 6, 5, 4, 3, 2, 1 };

	public static final AwaleMechanics mechanics = new AwaleMechanics();

	public static Player p1;
	public static Player p2;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		p1 = new Player("P1");
		p2 = new Player("P2");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testNormalMove() throws ImpossibleMoveException {
		Side s1 = new Side(p1, SETUP_NORMAL);
		Side s2 = new Side(p2, SETUP_EMPTY);

		mechanics.setSides(Arrays.asList(s1, s2));
		boolean replay = mechanics.moveTokens(s1, 0);
		List<Side> results = mechanics.getSides();

		int[] expected = { 0, 1, 1, 2, 0, 0 };
		assertEquals(2, results.size());

		assertArrayEquals(s2.getPits(), results.get(1).getPits());
		assertEquals(0, results.get(1).getBigPit());

		assertArrayEquals(expected, results.get(0).getPits());
		assertEquals(0, results.get(0).getBigPit());

		assertFalse(replay);
	}

	@Test
	public void testNormalMoveEndsBigPit() throws ImpossibleMoveException {
		Side s1 = new Side(p1, SETUP_BIGPIT);
		Side s2 = new Side(p2, SETUP_EMPTY);

		mechanics.setSides(Arrays.asList(s1, s2));
		boolean replay = mechanics.moveTokens(s1, 0);
		List<Side> results = mechanics.getSides();

		int[] expected = { 0, 1, 1, 2, 1, 1 };
		assertEquals(2, results.size());

		assertArrayEquals(s2.getPits(), results.get(1).getPits());
		assertEquals(0, results.get(1).getBigPit());

		assertArrayEquals(expected, results.get(0).getPits());
		assertEquals(1, results.get(0).getBigPit());

		assertTrue(replay);
	}

	@Test
	public void testNormalMoveEndsOtherPlayer() throws ImpossibleMoveException {

		Side s1 = new Side(p1, SETUP_OTHER_PLAYER);
		Side s2 = new Side(p2, SETUP_EMPTY);

		mechanics.setSides(Arrays.asList(s1, s2));
		boolean replay = mechanics.moveTokens(s1, 5);
		List<Side> results = mechanics.getSides();

		int[] expected = { 2, 0, 0, 0, 0, 0 };
		int[] expectedS2 = { 1, 1, 1, 1, 1, 1 };

		assertEquals(2, results.size());

		assertArrayEquals(expectedS2, results.get(1).getPits());
		assertEquals(0, results.get(1).getBigPit());

		assertArrayEquals(expected, results.get(0).getPits());
		assertEquals(1, results.get(0).getBigPit());

		assertFalse(replay);
	}

	@Test
	public void testMoveEndsEmptyPit() throws ImpossibleMoveException {
		Side s1 = new Side(p1, SETUP_ENDS_EMPTY);
		Side s2 = new Side(p2, SETUP_ENDS_EMPTY_P2);

		mechanics.setSides(Arrays.asList(s1, s2));
		boolean replay = mechanics.moveTokens(s1, 5);
		List<Side> results = mechanics.getSides();

		int[] expected = SETUP_EMPTY;
		int[] expected2 = { 1, 1, 1, 1, 1, 0 };

		assertEquals(2, results.size());

		assertArrayEquals(expected2, results.get(1).getPits());
		assertEquals(0, results.get(1).getBigPit());

		assertArrayEquals(expected, results.get(0).getPits());
		assertEquals(7, results.get(0).getBigPit());

		assertFalse(replay);
	}

	@Test(expected = ImpossibleMoveException.class)
	public void testPlayImpossibleNegativeMove() throws ImpossibleMoveException {
		Side s1 = new Side(p1, SETUP_ENDS_EMPTY);
		Side s2 = new Side(p2, SETUP_ENDS_EMPTY_P2);

		mechanics.setSides(Arrays.asList(s1, s2));
		mechanics.moveTokens(s1, -2);

	}

	@Test(expected = ImpossibleMoveException.class)
	public void testPlayImpossibleHighPitMove() throws ImpossibleMoveException {
		Side s1 = new Side(p1, SETUP_ENDS_EMPTY);
		Side s2 = new Side(p2, SETUP_ENDS_EMPTY_P2);
		mechanics.setSides(Arrays.asList(s1, s2));

		mechanics.moveTokens(s1, Side.NUMBER_PITS);
	}

	@Test(expected = ImpossibleMoveException.class)
	public void testPlayImpossibleEmptyPitMove() throws ImpossibleMoveException {
		Side s1 = new Side(p1, SETUP_EMPTY);
		Side s2 = new Side(p2, SETUP_EMPTY);
		mechanics.setSides(Arrays.asList(s1, s2));

		mechanics.moveTokens(s1, 1);
	}

	@Test
	public void testCleanPits() {
		Side s1 = new Side(p1, SETUP_CLEAN_P1);
		Side s2 = new Side(p2, SETUP_CLEAN_P2);
		mechanics.setSides(Arrays.asList(s1, s2));

		int sum1 = IntStream.of(SETUP_CLEAN_P1).sum();
		int sum2 = IntStream.of(SETUP_CLEAN_P2).sum();

		mechanics.cleanPits();
		assertEquals(sum1, mechanics.getSides().get(0).getBigPit());
		assertEquals(sum2, mechanics.getSides().get(1).getBigPit());

		assertEquals(0, mechanics.getSides().get(0).getPits()[0]);
		assertEquals(0, mechanics.getSides().get(0).getPits()[1]);
		assertEquals(0, mechanics.getSides().get(0).getPits()[2]);
		assertEquals(0, mechanics.getSides().get(0).getPits()[3]);
		assertEquals(0, mechanics.getSides().get(0).getPits()[4]);
		assertEquals(0, mechanics.getSides().get(0).getPits()[5]);

		assertEquals(0, mechanics.getSides().get(1).getPits()[0]);
		assertEquals(0, mechanics.getSides().get(1).getPits()[1]);
		assertEquals(0, mechanics.getSides().get(1).getPits()[2]);
		assertEquals(0, mechanics.getSides().get(1).getPits()[3]);
		assertEquals(0, mechanics.getSides().get(1).getPits()[4]);
		assertEquals(0, mechanics.getSides().get(1).getPits()[5]);
	}
}
