/**
 *
 */
package awale.board;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import awale.board.exceptions.GameNotFinishedException;
import awale.board.exceptions.ImpossibleMoveException;
import awale.board.player.Player;

/**
 * @author jll
 *
 */
public class BoardTest {

	public Board board;
	public Board emptyBoard;
	public ArrayList<Player> players;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		Player p1 = new Player("P1");
		Player p2 = new Player("P2");

		players = new ArrayList<>(Arrays.asList(p1, p2));
		board = new Board(players);
		emptyBoard = new Board(players, 0);

		board.ready();
		emptyBoard.ready();
	}

	@Test
	public void testUpdatePlayerTurn() throws ImpossibleMoveException {
		Player pBefore = board.getPlayerTurn();
		board.play(1);
		Player pAfter = board.getPlayerTurn();

		assertNotEquals(pAfter, pBefore);
	}

	@Test(expected = ImpossibleMoveException.class)
	public void testPlayImpossibleNegativeMove() throws ImpossibleMoveException {
		board.play(-2);
	}

	@Test(expected = ImpossibleMoveException.class)
	public void testPlayImpossibleHighPitMove() throws ImpossibleMoveException {
		board.play(Side.NUMBER_PITS);
	}

	@Test(expected = ImpossibleMoveException.class)
	public void testPlayImpossibleEmptyPitMove() throws ImpossibleMoveException {
		emptyBoard.play(Side.NUMBER_PITS);
	}

	@Test
	public void testIsTie() throws GameNotFinishedException {
		assertEquals(null, (emptyBoard.getWinner()));
	}

	@Test(expected = GameNotFinishedException.class)
	public void testIsGameFinished() throws GameNotFinishedException {
		assertEquals(null, (board.getWinner()));
	}

	@Test
	public void testGetNextPlayerTurn() throws ImpossibleMoveException {
		assertEquals(players.get(0), board.getPlayerTurn());
		assertEquals(players.get(1), board.getNextPlayerTurn());

		board.play(1);

		// We should have switched
		assertEquals(players.get(1), board.getPlayerTurn());
		assertEquals(players.get(0), board.getNextPlayerTurn());

	}

}
