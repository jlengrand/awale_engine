/**
 *
 */
package awale.board.player;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import awale.board.player.Player;

/**
 * Class testing the Player
 *
 * @author jleng
 *
 */
public class PlayerTest {

	public Player player;

	@Before
	public void setUp() {
		player = new Player();
	}

	@Test
	public void testAddToScore() {
		assertEquals(0, player.getScore());

		player.addToScore(-5);
		assertEquals(0, player.getScore());
	}

}
