/**
 *
 */
package server;
import static org.mockito.Mockito.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import java.net.SocketAddress;
import java.util.List;
import java.util.Map;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelConfig;
import org.jboss.netty.channel.ChannelEvent;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelHandler;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelSink;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.handler.codec.http.DefaultHttpRequest;
import org.jboss.netty.handler.codec.http.HttpHeaders.Names;
import org.jboss.netty.handler.codec.http.HttpHeaders.Values;
import org.jboss.netty.handler.codec.http.HttpMethod;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpResponse;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.jboss.netty.handler.codec.http.HttpVersion;
import org.jboss.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.jboss.netty.handler.codec.http.websocketx.WebSocketVersion;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import awale.server.AwaleServerHandler;
import awale.server.exceptions.UnknownMessageException;
import awale.server.messages.HandshakeMessage;

/**
 * Tests our custom Awale Channel Handler
 *
 * @author Julien Lengrand-Lambert
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class AwaleServerHandlerTest {

	private static final String WEBSOCKET_KEY = "i9ri`AfOgSsKwUlmLjIkGA==";

	@Mock
	ChannelHandlerContext mockContext;

	@Mock
	MessageEvent mockMessageEvent;
	
	@Mock
	Channel channel;
	
	@Mock
	ChannelPipeline pipeline;
	
	@Captor
	ArgumentCaptor<Channel> captor = ArgumentCaptor.forClass(Channel.class);

	private static final String TEST_URI = "http://www.example.com:8080";
	private static final String TEST_WS_URI = "/ws";
	private static final HttpRequest postHttpRequest = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.POST, TEST_URI);
	private static final HttpRequest getHttpRequestNoWS = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, TEST_URI);
	private HttpRequest getHttpRequest;
	
	public AwaleServerHandler handler;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		handler = new AwaleServerHandler();
		getHttpRequest = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, TEST_WS_URI);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test(expected = UnknownMessageException.class)
	public void testhandleUnknownRequest() throws Exception {
		MessageEvent message = Mockito.mock(MessageEvent.class);
		handler.messageReceived(null, message);
	}

	@Test
	public void testHandlePostHTTPRequest() throws Exception {

		when(mockContext.getChannel()).thenReturn(channel);
		when(mockMessageEvent.getMessage()).thenReturn(postHttpRequest);
		

		handler.messageReceived(mockContext, mockMessageEvent);
		verify(channel).write(captor.capture());
		
		final HttpResponse response = (HttpResponse) captor.getValue();
		assertEquals(HttpResponseStatus.FORBIDDEN, response.getStatus());
	}

	@Test
	public void testHandleGetHTTPRequestNoWebsocket() throws Exception {

		when(mockContext.getChannel()).thenReturn(channel);
		when(mockMessageEvent.getMessage()).thenReturn(getHttpRequestNoWS);

		handler.messageReceived(mockContext, mockMessageEvent);
		verify(channel).write(captor.capture());

		final HttpResponse response = (HttpResponse) captor.getValue();
		assertEquals(HttpResponseStatus.FORBIDDEN, response.getStatus());

	}

	@Test
	public void testHandleGetHTTPRequestWebsocketNoUpgrade() throws Exception {

		when(mockContext.getChannel()).thenReturn(channel);
		when(mockMessageEvent.getMessage()).thenReturn(getHttpRequest);

		handler.messageReceived(mockContext, mockMessageEvent);
		verify(channel).write(captor.capture());

		final HttpResponse response = (HttpResponse) captor.getValue();
		assertEquals(HttpResponseStatus.FORBIDDEN, response.getStatus());
	}

	@Test(expected = NullPointerException.class)
	public void testHandleGetHTTPRequestWebsocketNoVersion() throws Exception {

		when(mockContext.getChannel()).thenReturn(channel);
		when(mockMessageEvent.getMessage()).thenReturn(getHttpRequest);
		getHttpRequest.setHeader(Names.UPGRADE, Values.WEBSOCKET);
		getHttpRequest.setHeader(Names.CONNECTION, Values.UPGRADE);

		handler.messageReceived(mockContext, mockMessageEvent);
	}

	@Test
	public void testHandleGetHTTPRequestWebsocketUnknownVersion() throws Exception {

		when(mockContext.getChannel()).thenReturn(channel);
		when(mockMessageEvent.getMessage()).thenReturn(getHttpRequest);
		getHttpRequest.setHeader(Names.UPGRADE, Values.WEBSOCKET);
		getHttpRequest.setHeader(Names.CONNECTION, Values.UPGRADE);
		getHttpRequest.setHeader(Names.SEC_WEBSOCKET_VERSION, "Unknown");

		handler.messageReceived(mockContext, mockMessageEvent);
		verify(channel).write(captor.capture());

		final HttpResponse response = (HttpResponse) captor.getValue();
		assertEquals(HttpResponseStatus.UPGRADE_REQUIRED, response.getStatus());
	}

	@Test
	public void testHandleGetHTTPRequestWebsocket() throws Exception {

		when(mockContext.getChannel()).thenReturn(channel);
		when(mockMessageEvent.getMessage()).thenReturn(getHttpRequest);

		when(channel.getPipeline()).thenReturn(pipeline);

		getHttpRequest.setHeader(Names.UPGRADE, Values.WEBSOCKET);
		getHttpRequest.setHeader(Names.CONNECTION, Values.UPGRADE);
		getHttpRequest.setHeader(Names.SEC_WEBSOCKET_VERSION, WebSocketVersion.V13.toHttpHeaderValue());
		getHttpRequest.setHeader(Names.SEC_WEBSOCKET_KEY, WEBSOCKET_KEY);

		handler.messageReceived(mockContext, mockMessageEvent);
		verify(channel, times(2)).write(captor.capture());

		TextWebSocketFrame response = (TextWebSocketFrame) captor.getValue();

		HandshakeMessage message = new HandshakeMessage("Player 1");
		assertEquals(message.toJson(), response.getText());
	}
}
