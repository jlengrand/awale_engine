/**
 *
 */
package server.messages;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import com.google.gson.Gson;
import awale.server.messages.IllegalMoveMessage;

/**
 * @author Julien Lengrand-Lambert
 *
 */
public class IllegalMoveMessageTest {

	private final static String ILLEGAL_MESSAGE = "Cannot play a move on an empty pit.";
	IllegalMoveMessage message;
	Gson gson;

	@Before
	public void setUpBeforeClass() throws Exception {
		message = new IllegalMoveMessage(ILLEGAL_MESSAGE);
		gson = new Gson();

	}

	@Test
	public void test() {
		String jsonMessage = message.toJson();
		IllegalMoveMessage testMessage = gson.fromJson(jsonMessage, IllegalMoveMessage.class);
		assertEquals(IllegalMoveMessage.ILLEGAL_MESSAGE, testMessage.getMessageType());
		assertEquals(ILLEGAL_MESSAGE, testMessage.getInformationMessage());

	}

}
