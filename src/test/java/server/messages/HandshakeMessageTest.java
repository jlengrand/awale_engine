/**
 *
 */
package server.messages;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import com.google.gson.Gson;
import awale.board.player.Player;
import awale.server.messages.HandshakeMessage;

/**
 * @author Julien Lengrand-Lambert
 *
 */
public class HandshakeMessageTest {

	private static final String PLAYER_NAME = "Player 1";

	Player player = new Player(PLAYER_NAME);
	HandshakeMessage message = new HandshakeMessage(player.getName());
	Gson gson = new Gson();

	@Test
	public void testToJson() {
		String jsonMessage = message.toJson();

		HandshakeMessage testMessage = gson.fromJson(jsonMessage, HandshakeMessage.class);

		assertEquals(PLAYER_NAME, testMessage.getPlayer());
		assertEquals(HandshakeMessage.MESSAGE_TYPE, testMessage.getMessageType());
	}

}
