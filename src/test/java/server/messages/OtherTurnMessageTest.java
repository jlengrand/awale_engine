/**
 *
 */
package server.messages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import com.google.gson.Gson;
import awale.board.Board;
import awale.board.exceptions.GameAlreadyReadyException;
import awale.board.player.Player;
import awale.server.messages.OtherTurnMessage;

/**
 * @author Julien Lengrand-Lambert
 *
 */
public class OtherTurnMessageTest {

	Board board;
	OtherTurnMessage message;
	Gson gson;

	@Before
	public void setUpBeforeClass() throws Exception {
		board = new Board(new ArrayList<>(Arrays.asList(new Player("Player 1"), new Player("Player 2"))));
		message = new OtherTurnMessage(board);
		gson = new Gson();

		board.ready();

	}

	@Test
	public void testToJson() throws GameAlreadyReadyException {
		String jsonMessage = message.toJson();

		OtherTurnMessage testMessage = gson.fromJson(jsonMessage, OtherTurnMessage.class);

		assertEquals(OtherTurnMessage.MESSAGE_TYPE, testMessage.getMessageType());
		assertEquals(OtherTurnMessage.TURN_MESSAGE, testMessage.getTurnMessage());
		assertTrue(testMessage.getBoard().isReady());
		assertEquals("Player 1", testMessage.getBoard().getPlayerTurn().getName());
	}

}
