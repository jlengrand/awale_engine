# Awale game engine

A simple game engine that allows 2 players to play [Awale](http://www.myriad-online.com/resources/docs/awale/english/rules.htm).
The rules of the game are slightly different in this version, as per the instructions that were given to me.

I will not share them here.

## Technology stack

The game engine is written in Java 8, and the application can be built using Maven. 
The application uses websockets to let 2 players connect and play against each others.

Several libraries have been used to build this project

* Apache's commons-lang3
* Netty - latest version 3
* Google's Gson
* Grizzly - lastest to serve static files

For testing purposes only

* Junit, latest version 4
* Mockito-core latest version

The client side of the application uses (very) basic HTML, CSS and javascript.
The latest version of JQuery is used for simplicity purposes.

## Building the software

The build the engine, simply run 

```
mvn install
```

in the main folder of the repository.

## Running the software

Run 
```
java -jar awale-engine-0.0.1-SNAPSHOT-jar-with-dependencies.jar
```
 in the main folder of the repository.

 Once this is done, simply navigate to http://localhost:8080 with both clients